# Recordit URL Builder for Javascript

A simple tool to generate recordit URIs, it depends on and includes
the javascript SHA1 implementation by Chris Veness.

If you're using it in node environment, make sure you have node in your
project dependencies. If you're using it in the browser, make sure you're
including the neon source files beforehand.

npm package: https://www.npmjs.org/package/neon
github: https://github.com/azendal/neon

## Usage

```javascript
var urlBuilder = new Recordit.URLBuilder({
    clientID : "f33dd06d1cd1fb6e94504c767e5ccd9d0f7cd039",
    secret : "6d85ffe45dbc37f6e47bc443f6079dc26f61b3df"
});

urlBuilder.generate({
    fps : 12,
    encode : "none",
    action_url : "http://example.com/123",
    callback : "http://example.com/api/123",
    start_message : "This is the initial message",
    end_message : "This is the end message",
    width : 1280,
    height : 720
});

// => recordit:f33dd06d1cd1fb6e94504c767e5ccd9d0f7cd039-565fcfbfe0c2e0c59be5
9df7b3b73d42f564e6cc?fps=12&encode=none&action_url=http%3A%2F%2Fexample.com%
2F123&callback=http%3A%2F%2Fexample.com%2Fapi%2F123&start_message=This%20is%
20the%20initial%20message&end_message=This%20is%20the%20end%20message&width=
1280&height=720
```
